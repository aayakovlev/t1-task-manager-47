package ru.t1.aayakovlev.tm.exception.entity;

public final class UserEmailExistsException extends AbstractEntityException {

    public UserEmailExistsException() {
        super("Error! User with entered email already exists...");
    }

}
